<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <div class="container">
        <h2>Export dan Import Data Mahasiswa</h2>
        <hr>

        <a href="{{ route('export-mahasiswa') }}" class="btn btn-success mb-3">Export Data Mahasiswa</a>

        <form action="{{ route('import-mahasiswa') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="file">Import Data Mahasiswa (Excel)</label>
                <input type="file" class="form-control-file" id="file" name="file">
            </div>
            <button type="submit" class="btn btn-primary">Import</button>
        </form>
    </div>
</body>

</html>
