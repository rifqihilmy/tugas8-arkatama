<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('mahasiswa');
});

// routes/web.php
Route::get('/export-mahasiswa', [MahasiswaController::class, 'exportMahasiswa'])->name('export-mahasiswa');
Route::post('/import-mahasiswa', [MahasiswaController::class, 'importMahasiswa'])->name('import-mahasiswa');
