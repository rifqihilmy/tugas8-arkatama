<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $mahasiswa = [
            [
                'nama' => 'John Doe',
                'fakultas' => 'Fakultas Teknik',
                'prodi' => 'Teknik Informatika',
                'no_telpon' => '08123456789',
                'jenis_kelamin' => 'Laki-laki',
                'alamat' => 'Jl. Contoh Alamat 123',
                'tanggal_lahir' => '1990-01-15',
            ],
            [
                'nama' => 'Jane Smith',
                'fakultas' => 'Fakultas Ekonomi',
                'prodi' => 'Manajemen',
                'no_telpon' => '08567891234',
                'jenis_kelamin' => 'Perempuan',
                'alamat' => 'Jl. Alamat Lain 456',
                'tanggal_lahir' => '1992-03-20',
            ],
            // Tambahkan data mahasiswa lainnya sesuai kebutuhan
        ];

        // Looping untuk memasukkan data ke dalam tabel
        foreach ($mahasiswa as $data) {
            DB::table('mahasiswa')->insert([
                'nama' => $data['nama'],
                'fakultas' => $data['fakultas'],
                'prodi' => $data['prodi'],
                'no_telpon' => $data['no_telpon'],
                'jenis_kelamin' => $data['jenis_kelamin'],
                'alamat' => $data['alamat'],
                'tanggal_lahir' => $data['tanggal_lahir'],
            ]);
        }
    }
}
