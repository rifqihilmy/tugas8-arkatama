<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama',
        'fakultas',
        'prodi',
        'no_telpon',
        'jenis_kelamin',
        'alamat',
        'tanggal_lahir',
    ];
}
