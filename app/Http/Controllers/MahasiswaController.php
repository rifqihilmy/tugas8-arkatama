<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class MahasiswaController extends Controller
{
    public function exportMahasiswa()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $mahasiswa = DB::table('mahasiswa')->get();

        $sheet->setCellValue('A1', 'Nama Mahasiswa');
        $sheet->setCellValue('B1', 'Fakultas');
        $sheet->setCellValue('C1', 'Prodi');
        $sheet->setCellValue('D1', 'No Telepon');
        $sheet->setCellValue('E1', 'Jenis Kelamin');
        $sheet->setCellValue('F1', 'Alamat');
        $sheet->setCellValue('G1', 'Tanggal Lahir');

        $row = 2;

        foreach ($mahasiswa as $data) {
            $sheet->setCellValue('A' . $row, $data->nama);
            $sheet->setCellValue('B' . $row, $data->fakultas);
            $sheet->setCellValue('C' . $row, $data->prodi);
            $sheet->setCellValue('D' . $row, $data->no_telpon);
            $sheet->setCellValue('E' . $row, $data->jenis_kelamin);
            $sheet->setCellValue('F' . $row, $data->alamat);
            $sheet->setCellValue('G' . $row, $data->tanggal_lahir);
            $row++;
        }

        $writer = new Xlsx($spreadsheet);
        $filename = 'data_mahasiswa.xlsx';
        $writer->save($filename);

        return response()->download($filename)->deleteFileAfterSend(true);
    }

    public function importMahasiswa(Request $request)
    {
        $file = $request->file('file');

        $spreadsheet = IOFactory::load($file);
        $worksheet = $spreadsheet->getActiveSheet();
        $data = $worksheet->toArray();

        for ($i = 1; $i < count($data); $i++) {
            $mahasiswa = [
                'nama' => $data[$i][0],
                'fakultas' => $data[$i][1],
                'prodi' => $data[$i][2],
                'no_telpon' => $data[$i][3],
                'jenis_kelamin' => $data[$i][4],
                'alamat' => $data[$i][5],
                'tanggal_lahir' => $data[$i][6],
            ];
            DB::table('mahasiswa')->insert($mahasiswa);
        }

        return redirect()->back()->with('success', 'Data mahasiswa berhasil diimpor');
    }
}
